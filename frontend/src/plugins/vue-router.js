import Vue from 'vue';
import VueRouter from 'vue-router'
import ChartHistory from "../components/ChartHistory"
import ChartCreation from "../components/ChartCreation";

Vue.use(VueRouter);

const routes = [
    {path: '/history', component: ChartHistory},
    {path: '/', component: ChartCreation},
];

export default new VueRouter({routes});