from django.contrib import admin
from .models import Coordinate, Room


@admin.register(Coordinate)
class CoordinatesAdmin(admin.ModelAdmin):
    pass


@admin.register(Room)
class RoomsAdmin(admin.ModelAdmin):
    pass
