from django.shortcuts import render
from .models import Room, Coordinate
from .serializers import CoordinatesSerializer, RoomSerializers, BaseRoomSerializer
from rest_framework import generics


class CoordinateCreateAPIView(generics.CreateAPIView):
    queryset = Coordinate.objects.all()
    serializer_class = CoordinatesSerializer


class RoomListCreateAPIView(generics.ListCreateAPIView):
    queryset = Room.objects.all().order_by('-pk')
    list_serializer = RoomSerializers
    create_serializer = BaseRoomSerializer

    def get_serializer_class(self):
        if self.request.method == 'GET':
            return self.list_serializer
        else:
            return self.create_serializer


# Create your views here.
