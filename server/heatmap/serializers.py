from rest_framework import serializers
from .models import Coordinate, Room


class CoordinatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coordinate
        fields = ('id', 'coordinate_x', 'coordinate_y', 'value')


class BaseRoomSerializer(serializers.ModelSerializer):
    coordinates = CoordinatesSerializer(many=True)
    set_coordinates = serializers.SerializerMethodField()

    class Meta:
        model = Room
        fields = ('id', 'title', 'coordinates', 'set_coordinates')

    def create(self, validated_data):
        coordinates_validated_data = validated_data.pop('coordinates')
        room = Room.objects.create(**validated_data)
        coordinates_set_serializer = self.fields['coordinates']
        for each in coordinates_validated_data:
            each['room'] = room
        coordinate = coordinates_set_serializer.create(coordinates_validated_data)
        for item in coordinate:
            room.coordinates.add(item)
        return room

    def get_set_coordinates(self, obj):
        coordinates = obj.coordinates.all()
        all_x = list()
        all_y = list()
        for coordinate in coordinates:
            x = coordinate.coordinate_x
            y = coordinate.coordinate_y
            all_x.append(x)
            all_y.append(y)
        x_max = max(all_x)
        y_max = max(all_y)
        data = list()
        for _ in range(y_max):
            temp = list()
            for _ in range(x_max):
                temp.append(None)
            data.append(temp)
        for coordinate in coordinates:
            data[coordinate.coordinate_y - 1][coordinate.coordinate_x - 1] = coordinate.value
        return data


class RoomSerializers(serializers.ModelSerializer):
    coordinates = serializers.SerializerMethodField()

    class Meta:
        model = Room
        fields = ('id', 'title', 'coordinates')

    def get_coordinates(self, obj):
        coordinates = obj.coordinates.all()
        all_x = list()
        all_y = list()
        for coordinate in coordinates:
            x = coordinate.coordinate_x
            y = coordinate.coordinate_y
            all_x.append(x)
            all_y.append(y)
        x_max = max(all_x)
        y_max = max(all_y)
        data = list()
        for _ in range(y_max):
            temp = list()
            for _ in range(x_max):
                temp.append(None)
            data.append(temp)
        for coordinate in coordinates:
            data[coordinate.coordinate_y - 1][coordinate.coordinate_x - 1] = coordinate.value
        return data
