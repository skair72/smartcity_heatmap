from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from datetime import datetime


class Coordinate(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    coordinate_x = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(99)],
        verbose_name='Координата X'
    )
    coordinate_y = models.PositiveIntegerField(
        validators=[MinValueValidator(1), MaxValueValidator(99)],
        verbose_name='Координата Y')
    value = models.FloatField(verbose_name='Значение')

    class Meta:
        verbose_name = 'Координаты и значение датчика'
        verbose_name_plural = 'Координаты и значения датчиков'
        ordering = ['-created_at']

    def __str__(self):
        return '{} {} {}'.format(self.coordinate_x, self.coordinate_y, self.value)


class Room(models.Model):
    title = models.CharField('Название', max_length=100)
    coordinates = models.ManyToManyField(Coordinate, verbose_name='Координаты и значения датчиков')

    class Meta:
        verbose_name = 'Комната'
        verbose_name_plural = 'Комнаты'

    def __str__(self):
        return self.title
