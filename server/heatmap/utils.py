from .models import Room
import plotly.graph_objects as go


def convert_coordinates():
    rooms = Room.objects.all()
    rooms_coordinates = rooms['coordinates']
    all_x = list()
    all_y = list()
    for rooms_coordinate in rooms_coordinates:
        x = rooms_coordinate['coordinate_x']
        y = rooms_coordinate['coordinate_y']
        all_x.append(x)
        all_y.append(y)
    x_max = max(all_x)
    y_max = max(all_y)
    data = list()
    for _ in range(y_max):
        temp = list()
        for _ in range(x_max):
            temp.append(None)
        data.append(temp)
    for room in rooms:
        data[room['coordinate_y'] - 1][room['coordinate_x'] - 1] = room['value']
    return data


